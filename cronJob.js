var cron = require('node-cron');
var snmp = require('./snmp.js');
var dbOperations = require('./dbOperations.js');
var confSettingsObj = require('./settings.json');

/*
# ┌────────────── second (optional)
# │ ┌──────────── minute
# │ │ ┌────────── hour
# │ │ │ ┌──────── day of month
# │ │ │ │ ┌────── month
# │ │ │ │ │ ┌──── day of week
# │ │ │ │ │ │
# │ │ │ │ │ │
# * * * * * *
*/

var voltageValueList = [250,290,310];

var SNMPCron = cron.schedule('*/'+confSettingsObj.custom_snmp_job.frequency+' * * * *', function(){
  //snmp.sendSnmpTraps({"ip":"172.18.42.4","voltage_value": voltageValueList[Math.floor(Math.random() * voltageValueList.length)] });
  var confTrapObj = require('./settings.json');
  for (var i=0; i<confTrapObj.custom_snmp_job.trap_object.length; i++){
  	snmp.sendSnmpTraps(confTrapObj.custom_snmp_job.trap_object[i]);
  }
}, false);

var DynamicDataEntryCron = cron.schedule('*/'+confSettingsObj.dynamic_data_job.frequency+' * * * *', function(){
  dbOperations.BulkInsertDynamicData();
}, false);


if(confSettingsObj.custom_snmp_job.start == "true"){
	SNMPCron.start();
}
if(confSettingsObj.dynamic_data_job.start == "true"){
	DynamicDataEntryCron.start();
}
