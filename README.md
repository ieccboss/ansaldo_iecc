IECC Simulator
==============

It is designed to act like multiple IECC nodes.

Installation
-------------
After checking out the project from git cd to out folder

1. Install via Software installer.

  a. double click on 'iecc-simulator_0.0.1_all.deb'.
  b. follow the install instruction.

2. Install via apt-get

  a. 'sudo apt-get install <path/to/iecc-simulator_0.0.1_all.deb>'

Running IECC_Simulator
-----------------------
Run Simulator with root privileges.

  `sudo iecc-simulator`
