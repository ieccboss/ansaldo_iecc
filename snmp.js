var snmp = require ("net-snmp");
var fs = require('fs');
var path = require('path');
var ipListPath = path.join(__dirname, '.', 'ipList.json');
var objIP = JSON.parse(fs.readFileSync(ipListPath, 'utf8'));
var confSettingsObj = require('./settings.json');
var options = {
    port: 161,
    retries: 1,
    timeout: 5000,
    transport: "udp4",
    trapPort: 162,
    version: snmp.Version1
};
var session = snmp.createSession (confSettingsObj.boss_ip_address, "public", options);
var objSnmp = {};

var trapOid = "1.3.6.1.4.1.2000.1";
var objOids = {
    health_status : "1.3.6.1.2.1.1.1",
    latch_out_status : "1.3.6.1.2.1.1.2",
    aux_sensors_value : "1.3.6.1.2.1.1.3",
    aux_sensors_threshold : "1.3.6.1.2.1.1.4",
    motor_current_value : "1.3.6.1.2.1.1.5",
    motor_current_threshold : "1.3.6.1.2.1.1.6",
    motor_throw_time_value : "1.3.6.1.2.1.1.7",
    motor_throw_time_threshold : "1.3.6.1.2.1.1.8",
    voltage_value : "1.3.6.1.2.1.1.9"
}

objSnmp.sendSnmpTraps = function(_value){
    if (typeof global.numberOfIP !== 'undefined'){
    var key = Object.keys(_value)[1];
    var _oid = objOids[key];
    var varbinds = [
    {
        oid: _oid,
        type: snmp.ObjectType.OctetString,
        value: JSON.stringify(_value)
    }
    ];
    console.log(varbinds);
    session.trap (trapOid, varbinds, function (error) {
        if (error)
            console.error (error);
    });

}
}

module.exports = objSnmp;

