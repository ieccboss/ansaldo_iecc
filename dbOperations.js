var sqlite3 = require('sqlite3').verbose();
var path = require('path');
var dbPath = path.join(__dirname, '.', 'db', 'simulator.db');
var db = new sqlite3.Database(dbPath);
var fs = require('fs');
var snmp = require('./snmp.js')
var ipListPath = path.join(__dirname, '.', 'ipList.json');
var ObjDBOperations = {};
var connectionStatusList = ["connected", "disconnected"];
var healthStatusList = ["G","G","G","B"];
var latchOutStatusList = ["LO","NLO","NLO","NLO"];
var auxSensorsValueList = [20,21,22,25,35];
var motorCurrentValueList = [100,120,140,150,250];
var motorThrowTimeValueList = [10,11,12,13,16];
var voltageValueList = [2,3,6,7,9,9,9,10,12,15];
var confSettingsObj = require('./settings.json');

ObjDBOperations.getStaticData = function(ip, callback){
  db.each('SELECT * FROM tbl_iecc_node where assigned_ip ="' + ip + '"', function(err, row) {
    callback(err, row);
  });
}

ObjDBOperations.getDynamicData = function(ip, from_time,  callback){
  db.all('SELECT * FROM tbl_iecc_node_status where iecc_node_id = (SELECT iecc_node_id FROM tbl_iecc_node where assigned_ip ="' + ip + '") and created_on >= "' + from_time +'"', function(err,rows){
    callback(err, rows);
  });
}

ObjDBOperations.BulkInsertDynamicData = function(){
  var objIP = JSON.parse(fs.readFileSync(ipListPath, 'utf8'));
  if (typeof global.numberOfIP !== 'undefined'){
    db.serialize(function() {
      db.run("begin transaction");
      for (var i = 0; i < global.numberOfIP; i++) {
        var IP = objIP[i].ip;
          //db.run('insert into tbl_iecc_node_status values ((select iecc_node_id from tbl_iecc_node where assigned_ip = ?),?,?,?,?,"20V","100F","5A","100A",CURRENT_TIMESTAMP,"20V","20","20","20","50",CURRENT_TIMESTAMP,"30","abc abc abc abc abc","abc abc abc abc abc","E0014",CURRENT_TIMESTAMP)', objIP[i].ip, connectionStatusList[Math.floor(Math.random() * connectionStatusList.length)], statusList[Math.floor(Math.random() * statusList.length)], statusList[Math.floor(Math.random() * statusList.length)], "1" );
          db.run('update tbl_device_status set health_status = ?, latch_out_status = ?, aux_sensors_value = ?, motor_current_value = ?, motor_throw_time_value = ?, voltage_value = ? where ip = ?',  healthStatusList[Math.floor(Math.random() * healthStatusList.length)],  latchOutStatusList[Math.floor(Math.random() * latchOutStatusList.length)], auxSensorsValueList[Math.floor(Math.random() * auxSensorsValueList.length)], motorCurrentValueList[Math.floor(Math.random() * motorCurrentValueList.length)], motorThrowTimeValueList[Math.floor(Math.random() * motorThrowTimeValueList.length)], voltageValueList[Math.floor(Math.random() * voltageValueList.length)], objIP[i].ip, function(err){
            if(err)
              console.log(err);
          });
        }
        db.run("commit");
      });
    for (var j = 0; j < global.numberOfIP; j++) {
      db.all('select ip, health_status, latch_out_status, aux_sensors_value, aux_sensors_threshold, motor_current_value, motor_current_threshold, motor_throw_time_value, motor_throw_time_threshold, voltage_value from tbl_device_status where ip = ?', objIP[j].ip, function(error,rows, IP){
        if(error)
          console.log(err)
        else{
          Object.keys(rows[0]).forEach(function(key) {
            if(key !== "ip"){
              console.log(key);
              var trapValue = {"ip" : rows[0]['ip'], [key]: rows[0][key]};
              snmp.sendSnmpTraps(trapValue);
            }
          });
        }
      });
    }

    for (var k = 0; k < global.numberOfIP; k++) {
      db.all('select iecc_node_id as id from tbl_iecc_node where assigned_ip = ?', objIP[k].ip, function(error,rows){
        if(error)
          console.log(err)
        else{
          var db_eventLog = new sqlite3.Database(path.join(__dirname, '.', 'db/' + rows[0]['id'], 'events.db'));
          var db_maintenanceLog = new sqlite3.Database(path.join(__dirname, '.', 'db/' + rows[0]['id'], 'maintenance.db'));
          var db_swopLog = new sqlite3.Database(path.join(__dirname, '.', 'db/' + rows[0]['id'], 'swop.db'));
          //db_eventLog.run("PRAGMA journal_mode = WAL;");
          //db_swopLog.run("PRAGMA journal_mode = WAL;");
          //db_maintenanceLog.run("PRAGMA journal_mode = WAL;")
          db_eventLog.run('insert into events (source, primaryid, secondaryid, level, errtype, time, description) select source, primaryid, secondaryid, level, errtype, CURRENT_TIMESTAMP, description from events LIMIT ?', confSettingsObj.dynamic_data_job.new_records);
          db_swopLog.run('insert into swop (start, end, opertime, mcamps, temp, throwcount, time) select start, end, opertime, mcamps, temp, throwcount, CURRENT_TIMESTAMP from swop LIMIT ?', confSettingsObj.dynamic_data_job.new_records);
          db_maintenanceLog.run('insert into maintenance (LOG_DATE, INSPECTION_START_TIME, INSPECTION_END_TIME, INSPECTION_DURATION, INSPECTION_DESCRIPTION, INSPECTION_PERSONNEL, TIME_SINCE_LAST_INSPECTION) select LOG_DATE, INSPECTION_START_TIME, INSPECTION_END_TIME, INSPECTION_DURATION, INSPECTION_DESCRIPTION, CURRENT_TIMESTAMP, TIME_SINCE_LAST_INSPECTION from maintenance LIMIT ?', confSettingsObj.dynamic_data_job.new_records);
        }
      });
    }
  }
}

ObjDBOperations.GetSpecificStatus = function(status_field, ip, callback){
  var query = 'select ' + status_field + ' from tbl_device_status where ip = "' + ip + '"';
  db.all(query, function(err,rows){
    if(err)
      console.log('error '+ err);
    callback(rows);
  });
}

ObjDBOperations.GetSpecificField = function(fieldName, ip, callback){
  var query = 'select ' + fieldName + ' from tbl_iecc_node where assigned_ip = "' + ip + '"';
  db.all(query, function(err,rows){
    if(err)
      console.log('error '+ err);
    callback(rows);
  });
}

ObjDBOperations.getIdAndLatestCurrentFileName = function(ip,  callback){
db.all('SELECT iecc_node_id,latest_motor_current_file FROM tbl_iecc_node where assigned_ip ="' + ip + '"', function(err, row) {
      callback(err, row);
  });
}

ObjDBOperations.getLatestEventLog = function(dbPath, id, recordsLimit,  callback){
	var eventsDB = new sqlite3.Database(dbPath);
	eventsDB.all('SELECT * FROM events where id >"' + id + '" LIMIT ?', recordsLimit, function(err, row) {
      callback(err, row);
  });
}

ObjDBOperations.getLatestMaintenanceLog = function(dbPath, id, recordsLimit,  callback){
	var maintenanceDB = new sqlite3.Database(dbPath);
	maintenanceDB.all('SELECT * FROM maintenance where SWITCH_MACHINE_MAINTENANCE_NUM >"' + id + '" LIMIT ?', recordsLimit, function(err, row) {
      callback(err, row);
  });
}

ObjDBOperations.getLatestSwopLog = function(dbPath, id, recordsLimit, callback){
	var swopDB = new sqlite3.Database(dbPath);
	swopDB.all('SELECT * FROM swop where id >"' + id + '" LIMIT ?', recordsLimit, function(err, row) {
      callback(err, row);
  });
}

ObjDBOperations.UpdateSubnetMask = function(newSubnetMask, oldSubnetMask, callback){
  db.serialize(function() {
    db.run("begin transaction");
    var query1 = "UPDATE tbl_device_status SET ip = replace( ip, '" + oldSubnetMask + "', '"+ newSubnetMask +"' ) WHERE ip LIKE '" + oldSubnetMask + "%';"
      db.run(query1, function(err){
          if(err)
              console.log(err);
          else{
            var query2 = "UPDATE tbl_iecc_node SET assigned_ip = replace( assigned_ip, '" + oldSubnetMask + "', '"+ newSubnetMask +"' ) WHERE assigned_ip LIKE '" + oldSubnetMask + "%';"
            db.run(query2, function(err){
              if(err)
                console.log(err);
              else{
                callback();
              }
            });
          }
      });
    db.run("commit");
  });
}

module.exports = ObjDBOperations;